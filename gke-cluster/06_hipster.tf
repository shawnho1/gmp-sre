module "hipster" {
  source = "terraform-google-modules/gcloud/google//modules/kubectl-wrapper"

  project_id              = data.google_project.project.project_id
  cluster_name            = module.gke.name
  cluster_location        = module.gke.location
  module_depends_on       = [module.gke]
  kubectl_create_command  = "kubectl apply -f hipster.yaml -n hipster && kubectl apply -f istio-hipster.yaml -n hipster"
  kubectl_destroy_command = "kubectl delete -f hipster.yaml -n hipster && kubectl delete -f istio-hipster.yaml -n hipster"
}