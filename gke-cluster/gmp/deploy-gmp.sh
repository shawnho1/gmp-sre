#!/bin/bash
export PROJECT_ID="shawnho-demo-2023"
export CLUSTER_NAME="gke-cluster"
export REGION="asia-east1"

function __main__ () {
    __create_cadvisor_scraper__
    __create_asm_scraper__
    __create_kube_state_metric__
    __create_node_scraper__ 
    __create_promtheus_scraper__
}
function __deploy_prometheus_interface__ () {
    gcloud config set project ${PROJECT_ID}
    gcloud iam service-accounts create gmp-test-sa
    gcloud iam service-accounts add-iam-policy-binding \
    --role roles/iam.workloadIdentityUser \
    --member "serviceAccount:${PROJECT_ID}.svc.id.goog[gmp-public/default]" \
    gmp-test-sa@${PROJECT_ID}.iam.gserviceaccount.com
    kubectl annotate serviceaccount \
    --namespace gmp-public \
    default \
    iam.gke.io/gcp-service-account=gmp-test-sa@${PROJECT_ID}.iam.gserviceaccount.com
    gcloud projects add-iam-policy-binding {PROJECT_ID} \
    --member=serviceAccount:gmp-test-sa@{PROJECT_ID}.iam.gserviceaccount.com \
    --role=roles/monitoring.viewer
    kubectl apply -f frontend.yaml
}
function __create_gmp_operator__ () {
    kubectl apply -f ./setup.yaml
    kubectl apply -f ./operator.yaml
}
function __create_promtheus_scraper__ () {
    kubectl apply -f prometheus_scraper.yaml
}
function __create_cadvisor_scraper__ () {
cat <<-EOF > cadvisor_scraper.yaml
apiVersion: monitoring.googleapis.com/v1
kind: OperatorConfig
managedAlertmanager:
  configSecret:
    key: alertmanager.yaml
    name: alertmanager
metadata:
  namespace: gmp-public
  name: config
collection:
  kubeletScraping:
    interval: 30s
EOF
kubectl apply -f cadvisor_scraper.yaml
}

function __create_asm_scraper__ () {
    kubectl apply -f asm_scraper.yaml
}

function __update_gke_control_plane__ () {
    gcloud container clusters update ${CLUSTER_NAME} \
    --region=${REGION} \
    --monitoring=SYSTEM,API_SERVER,SCHEDULER,CONTROLLER_MANAGER
}

function __create_kube_state_metric__ () {
    kubectl apply -f kube-state-metric.yaml    
}
function __create_node_scraper__ () {
    kubectl apply -f node-exporter.yaml
}
# Run the script from main()
__main__ "$@"

# # create serviceaccount for GMP for non-GKE environment
# gcloud iam service-accounts create gmp-test-sa
# gcloud projects add-iam-policy-binding ${PROJECT_ID}\
#   --member=serviceAccount:gmp-test-sa@${PROJECT_ID}.iam.gserviceaccount.com \
#   --role=roles/monitoring.metricWriter

# gcloud iam service-accounts keys create gmp-test-sa-key.json \
#   --iam-account=gmp-test-sa@${PROJECT_ID}.iam.gserviceaccount.com
# kubectl -n gmp-public create secret generic gmp-test-sa \
#   --from-file=key.json=gmp-test-sa-key.json

# rm gmp-test-sa-key.json

# cat << EOF > operator_config.yaml
# apiVersion: monitoring.googleapis.com/v1
# kind: OperatorConfig
# metadata:
#   namespace: gmp-public
#   name: config
# collection:
#   credentials:
#     name: gmp-test-sa
#     key: key.json
# EOF

