resource "null_resource" "cloud-workstation" {
    triggers = {
        project_id = var.project_id
        region = var.region
        vpc_name = data.google_compute_network.host_network.id
        subnet_name = data.google_compute_subnetwork.host_subnetwork.id
        ws_cluster_name = var.ws_cluster_name
    }
    provisioner "local-exec" {
        command = "${path.module}/install-cloudworkstation.sh"
        environment = {
            PROJECT = var.project_id
            REGION  = var.region
            NETWORK = "${data.google_compute_network.host_network.id}"
            SUBNET  = "${data.google_compute_subnetwork.host_subnetwork.id}"
            WS_CLUSTER_NAME = var.ws_cluster_name
        }
    }
}