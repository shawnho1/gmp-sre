module "asm" {
  source            = "terraform-google-modules/kubernetes-engine/google//modules/asm"
  project_id        = data.google_project.project.project_id
  cluster_name      = module.gke.name
  cluster_location  = module.gke.location
#  internal_ip       = true
  fleet_id          = module.fleet.cluster_membership_id 
}
