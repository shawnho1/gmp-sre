resource "google_service_account" "anthos-admin" {
  for_each = local.projects
  project    = each.value.id
  account_id   = "anthos-admin"
  display_name = "A service account for anthos-admin"
}
resource "google_project_iam_binding" "project" {
  for_each = local.projects
  project    = each.value.id
  role    = "roles/owner"

  members = [
    google_service_account.anthos-admin[each.key].member
  ]
}