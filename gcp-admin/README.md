# Argolis Project Creation

## Why this project?
There are some organization policies which should be modified to better utilize argolis. I therefore create this project to increase the efficiencies. 

## How to use this project?
* Download this folder into your workstation with terraform CLI. Login with your argolis administrator account on your workstation with the following command.
```
gcloud auth application-default login
```
* Modify the project_id in terraform.tfvars to the project_name your desired. Noted that the current project name and project_id are identical. 
* terraform init && terraform apply 
* After the project is created, you could add your google.com account using Cloud Console.
* Relogin with your google.com account.