#!/bin/bash
export CLUSTER_ID=cluster1
export KUBECONFIG_PATH_IN_ADMIN_VM=/home/tfadmin/bmctl-workspace/$CLUSTER_ID/$CLUSTER_ID-kubeconfig
# pick a valid path in your local workstation
export KUBECONFIG_PATH_IN_LOCAL_WORKSTATION=/Users/shawnho/.kube/abm-kubeconfig
export PROJECT=shawn-demo-2023
export ZONE=us-central1-a


gcloud compute scp \
    --project=${PROJECT} \
    --zone=${ZONE} \
    tfadmin@${CLUSTER_ID}-abm-ws0-001:${KUBECONFIG_PATH_IN_ADMIN_VM} \
    ${KUBECONFIG_PATH_IN_LOCAL_WORKSTATION}

export KUBECONFIG=$KUBECONFIG_PATH_IN_LOCAL_WORKSTATION
kubectl get nodes
