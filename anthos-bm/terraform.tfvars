project_id                    = "shawnho-demo-2023"
host_project_id               = "shawnho-netadmin-2023"
region                        = "asia-east1"
zone                          = "asia-east1-a"
credentials_file              = "/Users/shawnho/gcp-keys/shawnho-demo-2023-sa.key"
gce_vm_service_account        = "anthos-admin@shawnho-demo-2023.iam.gserviceaccount.com"
network                       = "simulated"
subnetwork                    = "simulated"
mode                          = "install"
#mode                          = "manuallb"
abm_cluster_id                = "cluster1"
anthos_service_account_name   = "baremetal-gcr"
resources_path                = "./resources"
machine_type                  = "n1-standard-4"
instance_count                = {
                                  "controlplane" : 3
                                  "worker" : 3
                                }
#gcp_login_accounts           = ["<GCP_ACCOUNT_1>", <GCP_ACCOUNT_2>, <GCP_ACCOUNT_3>]
#gce_vm_service_account       = "<GCE_SERVICE_ACCOUNT_WITH_iam.serviceAccountKeyAdmin_PERMISSION>"
#nfs_server                   = true
#gpu                          = {
#                                 count = 1,
#                                 type = "nvidia-tesla-k80"
#                               }
