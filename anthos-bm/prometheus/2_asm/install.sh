#!/bin/bash
source ../env.sh

function __main__ () {
  echo "[$DATE] Init script running"
  __print_separator__
  #__install_asm__
  #__install_istio_ingressgateway__
  __install_kiali__
  __print_separator__
  __install_istio_scraper__
  echo "[+] Successfully completed initialization"
}

function __create_ns_istio__ () {
  ns=$1
  kubectl create $ns
  kubectl label ns $ns istio.io/rev=${ASM_LABEL}
}

function __deploy_sample__ () {
  namespace="hipster"
  __create_ns_istio__ ${namespace}
  kubectl apply -f samples/hipster -n ${namespace}
  kubectl apply -f ./istio-sidecar.yaml -n ${namespace}
  __check_exit_status__ $? \
    "[+] Successfully deploy hipster samples" \
    "[-] Failed to deploy hipster samples. "
  __print_separator__
}

# install ASM on-premise
function __install_asm__ () {
  ./asmcli install \
    --fleet_id ${FLEET_ID} \
    --kubeconfig ${KUBECONFIG_PATH} \
    --output_dir ${ASM_DIR_PATH} \
    --platform multicloud \
    --enable_all \
    --ca mesh_ca \
    --option prometheus

  __check_exit_status__ $? \
    "[+] Successfully install & config ASM" \
    "[-] Failed to install & config ASM "
  __print_separator__
}
# Deploy Istio ServiceMonitor - cluster-wide crd
function __install_istio_scraper__ () {
  kubectl apply -f istio-prometheus-scrape.yaml
}
function __install_istio_ingressgateway__ () {
    # install istio-ingressgateway
  __create_ns__
  kubectl apply -f istio-ingressgateway/ -n gateway
}
# install Kiali but refer to Prometheus
function __install_kiali__ () {
  __gen_kiali_yaml__
  kubectl apply -f kiali/kiali-cm.yaml -n istio-system
  kubectl apply -f kiali/kiali.yaml -n istio-system
  __check_exit_status__ $? \
    "[+] Successfully install Kiali" \
    "[-] Failed to install Kiali"
  __print_separator__
}
# generate configmap for Kiali
function __gen_kiali_yaml__ () {
cat << EOF > ./kiali/kiali-cm.yaml
# Source: kiali-server/templates/configmap.yaml
apiVersion: v1
kind: ConfigMap
metadata:
  name: kiali
  namespace: istio-system
  labels:
    helm.sh/chart: kiali-server-1.50.0
    app: kiali
    app.kubernetes.io/name: kiali
    app.kubernetes.io/instance: kiali
    version: "v1.50.0"
    app.kubernetes.io/version: "v1.50.0"
    app.kubernetes.io/managed-by: Helm
    app.kubernetes.io/part-of: "kiali"
data:
  config.yaml: |
    auth:
      openid: {}
      openshift:
        client_id_prefix: kiali
      strategy: anonymous
    deployment:
      accessible_namespaces:
      - '**'
      additional_service_yaml: {}
      affinity:
        node: {}
        pod: {}
        pod_anti: {}
      configmap_annotations: {}
      custom_secrets: []
      host_aliases: []
      hpa:
        api_version: autoscaling/v2beta2
        spec: {}
      image_digest: ""
      image_name: quay.io/kiali/kiali
      image_pull_policy: Always
      image_pull_secrets: []
      image_version: v1.50
      ingress:
        additional_labels: {}
        class_name: nginx
        override_yaml:
          metadata: {}
      ingress_enabled: false
      instance_name: kiali
      logger:
        log_format: text
        log_level: info
        sampler_rate: "1"
        time_field_format: 2006-01-02T15:04:05Z07:00
      namespace: istio-system
      node_selector: {}
      pod_annotations: {}
      pod_labels:
        sidecar.istio.io/inject: "false"
      priority_class_name: ""
      replicas: 1
      resources:
        limits:
          memory: 1Gi
        requests:
          cpu: 10m
          memory: 64Mi
      secret_name: kiali
      service_annotations: {}
      service_type: ""
      tolerations: []
      version_label: v1.50.0
      view_only_mode: false
    external_services:
      custom_dashboards:
        enabled: true
      istio:
        config_map_name: "istio-${ASM_LABEL}"
        istio_sidecar_injector_config_map_name: "istio-sidecar-injector-${ASM_LABEL}"
        istiod_deployment_name: "istiod-${ASM_LABEL}"
        root_namespace: istio-system
      prometheus:
        url: ${PROMETHEUS_SERVICE}
      grafana:
        enabled: true
        in_cluster_url: ${GRAFANA_SERVICE}
      tracing:
        enabled: true
        in_cluster_url: 'http://tracing.istio-system.svc:16685/jaeger'
        use_grpc: true
    identity:
      cert_file: ""
      private_key_file: ""
    istio_namespace: istio-system
    kiali_feature_flags:
      certificates_information_indicators:
        enabled: true
        secrets:
        - cacerts
        - istio-ca-secret
      clustering:
        enabled: true
      disabled_features: []
      validations:
        ignore:
        - KIA1201
    login_token:
      signing_key: CHANGEME00000000
    server:
      metrics_enabled: true
      metrics_port: 9090
      port: 20001
      web_root: /kiali
EOF
}

function __print_separator__ () {
  echo "------------------------------------------------------------------------------"
}

# Run the script from main()
__main__ "$@"