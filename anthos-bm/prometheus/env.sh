#!/bin/bash
export ROOT=$(pwd)
export FLEET_ID="shawn-mesh-2022"
export USER="shawnho@google.com"
export MONITORING_NS="monitoring"
export ASM_DIR_PATH="./asm-install"
export ASM_LABEL="asm-1145-3"
export KUBECONFIG_PATH="/home/tfadmin/.kube/config"
export PROMETHEUS_SERVICE="http://prometheus-kube-prometheus-prometheus.monitoring.svc.cluster.local:9090"
export GRAFANA_SERVICE="http://prometheus-grafana.monitoring.svc.cluster.local:80"

function __check_exit_status__ () {
  EXIT_CODE=$1
  SUCCESS_MSG=$2
  FAILURE_MSG=$3

  if [ "$EXIT_CODE" -eq 0 ]
  then
    echo "$SUCCESS_MSG"
  else
    echo "$FAILURE_MSG" >&2
    exit "$EXIT_CODE"
  fi
}