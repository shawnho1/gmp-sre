#!/bin/bash
function __main__ () {
    #__config_google_credentials__
    #__config_workload_identity__
    #__delete_workload_identity__
    __upgrade_prometheus__
}
function __config_google_credentials__ () {
    kubectl -n monitoring create secret generic gmp-test-sa --from-file=key.json=/Users/shawnho/gcp-keys/shawn-mesh-2022-metric-writer.key
}
function __upgrade_prometheus__ () {
    helm upgrade prometheus -n monitoring -f ./values.yaml .
}
function __install_prometheus__ () {
    helm install prometheus -n monitoring -f ./values.yaml .
}
function __config_workload_identity__ () {
    echo "config workload identity..."
    gcloud iam service-accounts add-iam-policy-binding gmp-test@shawn-mesh-2022.iam.gserviceaccount.com \
    --role roles/iam.workloadIdentityUser \
    --member "serviceAccount:shawn-mesh-2022.svc.id.goog[monitoring/prometheus-kube-prometheus-prometheus]"

    kubectl annotate serviceaccount \
    --namespace monitoring \
    prometheus-kube-prometheus-prometheus \
    iam.gke.io/gcp-service-account=gmp-test@shawn-mesh-2022.iam.gserviceaccount.com
}

function __delete_workload_identity__ () {
    echo "delete workload identity..."
    gcloud iam service-accounts remove-iam-policy-binding gmp-test@shawn-mesh-2022.iam.gserviceaccount.com \
    --role roles/iam.workloadIdentityUser \
    --member "serviceAccount:shawn-mesh-2022.svc.id.goog[monitoring/prometheus-kube-prometheus-prometheus]"
    kubectl annotate serviceaccount prometheus-kube-prometheus-prometheus -n monitoring iam.gke.io/gcp-service-account-
}
# Run the script from main()
__main__ "$@"