#!/bin/bash
source ../env.sh

# Enable API
gcloud services enable --project=${PROJECT_ID}  \
    connectgateway.googleapis.com \
    anthos.googleapis.com \
    gkeconnect.googleapis.com \
    gkehub.googleapis.com \
    cloudresourcemanager.googleapis.com

# Setup IAM
gcloud projects add-iam-policy-binding ${PROJECT_ID} \
   --member user:${USER} \
   --role='roles/serviceusage.serviceUsageAdmin'
gcloud projects add-iam-policy-binding ${PROJECT_ID} \
   --member user:${USER} \
   --role='roles/resourcemanager.projectIamAdmin'

gcloud projects add-iam-policy-binding ${PROJECT_ID} \
    --member=user:${USER} \
    --role=roles/gkehub.gatewayAdmin
gcloud projects add-iam-policy-binding ${PROJECT_ID} \
    --member=user:${USER} \
    --role=roles/gkehub.viewer

gcloud container fleet memberships generate-gateway-rbac  \
    --membership=cluster1 \
    --role=clusterrole/cluster-admin \
    --users=${USER} \
    --project=${PROJECT_ID} \
    --kubeconfig=./kubeconfig \
    --context=cluster1-admin@cluster1 \
    --apply


