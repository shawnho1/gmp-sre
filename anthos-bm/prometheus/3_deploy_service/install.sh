#!/bin/bash
source ../env.sh
function __main__ () {
  echo "[$DATE] Init script running"
  __print_separator__
  namespace="whereami"
  #__install_sample__ $namespace
  __install_namespace_monitor__ $namespace
  echo "[+] Successfully completed initialization"
}

function __install_sample__ () {
  namespace=$1
  __create_ns_istio__ $namespace 
  kubectl apply -k ../2_asm/samples/${namespace}/backend -n ${namespace}
  kubectl apply -k ../2_asm/samples/${namespace}/frontend -n ${namespace}
  __check_exit_status__ $? \
    "[+] Successfully deploy ${namespace} samples" \
    "[-] Failed to deploy ${namespace} samples. "
  __print_separator__    
}

function __install_namespace_monitor__ () {
  namespace=$1
  kubectl apply -f ./istio-sidecar.yaml -n ${namespace}
  __gen_prom_proxy_yaml__ ${namespace}
  kubectl apply -f prom-proxy/ -n $namespace

  __gen_kiali_yaml__ $namespace
  __install_kiali__ $namespace
  __check_exit_status__ $? \
    "[+] Successfully deploy ${namespace} monitoring" \
    "[-] Failed to deploy ${namespace} monitoring. "
  __print_separator__    
}

function __create_ns_istio__ () {
  ns=$1
  kubectl create ns $ns
  kubectl label ns $ns istio.io/rev=${ASM_LABEL}
  kubectl apply -f ./istio-sidecar.yaml -n $ns
}

# install Kiali but refer to Prometheus
function __install_kiali__ () {
  namespace=$1
  __gen_kiali_yaml__ $namespace
  kubectl apply -f kiali/kiali-all.yaml -n istio-system
  __check_exit_status__ $? \
    "[+] Successfully install Kiali in ${namespace}" \
    "[-] Failed to install Kiali in ${namespace}"
  __print_separator__
}
# generate configmap for Kiali
function __gen_kiali_yaml__ () {
  ns=$1
  PROMETHEUS_PROXY_SERVICE="http://prometheus-kube-prometheus-prometheus.monitoring.svc.cluster.local:9090"
cat << EOF > ./kiali/kiali-all.yaml
# Source: kiali-server/templates/configmap.yaml
apiVersion: v1
kind: ConfigMap
metadata:
  name: kiali
  labels:
    helm.sh/chart: kiali-server-1.50.0
    app: kiali
    app.kubernetes.io/name: kiali
    app.kubernetes.io/instance: kiali
    version: "v1.50.0"
    app.kubernetes.io/version: "v1.50.0"
    app.kubernetes.io/managed-by: Helm
    app.kubernetes.io/part-of: "kiali"
data:
  config.yaml: |
    auth:
      openid: {}
      openshift:
        client_id_prefix: kiali
      strategy: anonymous
    deployment:
      accessible_namespaces:
      - "whereami"
      - "istio-system"
      additional_service_yaml: {}
      affinity:
        node: {}
        pod: {}
        pod_anti: {}
      configmap_annotations: {}
      custom_secrets: []
      host_aliases: []
      hpa:
        api_version: autoscaling/v2beta2
        spec: {}
      image_digest: ""
      image_name: quay.io/kiali/kiali
      image_pull_policy: Always
      image_pull_secrets: []
      image_version: v1.50
      ingress:
        additional_labels: {}
        class_name: nginx
        override_yaml:
          metadata: {}
      ingress_enabled: false
      instance_name: kiali
      logger:
        log_format: text
        log_level: info
        sampler_rate: "1"
        time_field_format: 2006-01-02T15:04:05Z07:00
      namespace: istio-system
      node_selector: {}
      pod_annotations: {}
      pod_labels:
        sidecar.istio.io/inject: "false"
      priority_class_name: ""
      replicas: 1
      resources:
        limits:
          memory: 1Gi
        requests:
          cpu: 10m
          memory: 64Mi
      secret_name: kiali
      service_annotations: {}
      service_type: ""
      tolerations: []
      version_label: v1.50.0
      view_only_mode: false
    external_services:
      custom_dashboards:
        enabled: true
      istio:
        config_map_name: "istio-${ASM_LABEL}"
        istio_sidecar_injector_config_map_name: "istio-sidecar-injector-${ASM_LABEL}"
        istiod_deployment_name: "istiod-${ASM_LABEL}"
        root_namespace: istio-system
      prometheus:
        url: ${PROMETHEUS_PROXY_SERVICE}
      grafana:
        enabled: false
        in_cluster_url: ${GRAFANA_SERVICE}
      tracing:
        enabled: true
        in_cluster_url: 'http://tracing.hipster.svc:16685/jaeger'
        use_grpc: true
    identity:
      cert_file: ""
      private_key_file: ""
    istio_namespace: istio-system
    kiali_feature_flags:
      certificates_information_indicators:
        enabled: true
        secrets:
        - cacerts
        - istio-ca-secret
      clustering:
        enabled: true
      disabled_features: []
      validations:
        ignore:
        - KIA1201
    login_token:
      signing_key: CHANGEME00000000
    server:
      metrics_enabled: true
      metrics_port: 9090
      port: 20001
      web_root: /kiali
---
apiVersion: v1
kind: Service
metadata:
  name: kiali-${ns}
  labels:
    helm.sh/chart: kiali-server-1.50.0
    app: kiali-${ns}
    app.kubernetes.io/name: kiali-${ns}
    app.kubernetes.io/instance: kiali-${ns}
    version: "v1.50.0"
    app.kubernetes.io/version: "v1.50.0"
    app.kubernetes.io/managed-by: Helm
    app.kubernetes.io/part-of: "kiali-${ns}"
spec:
  type: LoadBalancer
  ports:
  - name: http
    protocol: TCP
    port: 20001
  - name: http-metrics
    protocol: TCP
    port: 9090
  selector:
    app.kubernetes.io/name: kiali-${ns}
    app.kubernetes.io/instance: kiali-${ns}
---
# Source: kiali-server/templates/deployment.yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: kiali-${ns}
  labels:
    helm.sh/chart: kiali-server-1.50.0
    app: kiali-${ns}
    app.kubernetes.io/name: kiali-${ns}
    app.kubernetes.io/instance: kiali-${ns}
    version: "v1.50.0"
    app.kubernetes.io/version: "v1.50.0"
    app.kubernetes.io/managed-by: Helm
    app.kubernetes.io/part-of: kiali-${ns}
spec:
  replicas: 1
  selector:
    matchLabels:
      app.kubernetes.io/name: kiali-${ns}
      app.kubernetes.io/instance: kiali-${ns}
  strategy:
    rollingUpdate:
      maxSurge: 1
      maxUnavailable: 1
    type: RollingUpdate
  template:
    metadata:
      name: kiali-${ns}
      labels:
        helm.sh/chart: kiali-server-1.50.0
        app: kiali-${ns}
        app.kubernetes.io/name: kiali-${ns}
        app.kubernetes.io/instance: kiali-${ns}
        version: "v1.50.0"
        app.kubernetes.io/version: "v1.50.0"
        app.kubernetes.io/managed-by: Helm
        app.kubernetes.io/part-of: kiali-${ns}
        sidecar.istio.io/inject: "false"
      annotations:
        checksum/config: 03a5118048208a4375d255f2e6119911359cbdf0bbbea7a66012cf2648b41d52
        prometheus.io/scrape: "true"
        prometheus.io/port: "9090"
        kiali.io/dashboards: go,kiali
    spec:
      serviceAccountName: kiali
      containers:
      - image: "quay.io/kiali/kiali:v1.50"
        imagePullPolicy: Always
        name: kiali
        command:
        - "/opt/kiali/kiali"
        - "-config"
        - "/kiali-configuration/config.yaml"
        securityContext:
          allowPrivilegeEscalation: false
          privileged: false
          readOnlyRootFilesystem: true
          runAsNonRoot: true
        ports:
        - name: api-port
          containerPort: 20001
        - name: http-metrics
          containerPort: 9090
        readinessProbe:
          httpGet:
            path: /kiali/healthz
            port: api-port
            scheme: HTTP
          initialDelaySeconds: 5
          periodSeconds: 30
        livenessProbe:
          httpGet:
            path: /kiali/healthz
            port: api-port
            scheme: HTTP
          initialDelaySeconds: 5
          periodSeconds: 30
        env:
        - name: ACTIVE_NAMESPACE
          valueFrom:
            fieldRef:
              fieldPath: metadata.namespace
        - name: LOG_LEVEL
          value: "info"
        - name: LOG_FORMAT
          value: "text"
        - name: LOG_TIME_FIELD_FORMAT
          value: "2006-01-02T15:04:05Z07:00"
        - name: LOG_SAMPLER_RATE
          value: "1"
        volumeMounts:
        - name: kiali-configuration
          mountPath: "/kiali-configuration"
        - name: kiali-cert
          mountPath: "/kiali-cert"
        - name: kiali-secret
          mountPath: "/kiali-secret"
        - name: kiali-cabundle
          mountPath: "/kiali-cabundle"
        resources:
          limits:
            memory: 1Gi
          requests:
            cpu: 10m
            memory: 64Mi
      volumes:
      - name: kiali-configuration
        configMap:
          name: kiali
      - name: kiali-cert
        secret:
          secretName: istio.kiali-service-account
          optional: true
      - name: kiali-secret
        secret:
          secretName: kiali
          optional: true
      - name: kiali-cabundle
        configMap:
          name: kiali-cabundle
          optional: true
EOF
}

function __gen_prom_proxy_yaml__ () {
    ns=$1
cat << EOF > prom-proxy/prom-proxy-dep.yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  annotations:
    deployment.kubernetes.io/revision: "1"
    meta.helm.sh/release-name: prom-proxy-default
    meta.helm.sh/release-namespace: default
  labels:
    app.kubernetes.io/instance: prom-proxy-default
    app.kubernetes.io/managed-by: Helm
    app.kubernetes.io/name: prom-label-proxy
    app.kubernetes.io/version: v0.5.0
    helm.sh/chart: prom-label-proxy-0.1.0
  name: prom-proxy-${ns}
  namespace: ${ns}
spec:
  progressDeadlineSeconds: 600
  replicas: 1
  revisionHistoryLimit: 10
  selector:
    matchLabels:
      app.kubernetes.io/instance: prom-proxy
      app.kubernetes.io/name: prom-label-proxy
  strategy:
    rollingUpdate:
      maxSurge: 25%
      maxUnavailable: 25%
    type: RollingUpdate
  template:
    metadata:
      creationTimestamp: null
      labels:
        app.kubernetes.io/instance: prom-proxy
        app.kubernetes.io/name: prom-label-proxy
    spec:
      containers:
      - args:
        - --insecure-listen-address=0.0.0.0:8080
        - --upstream=${PROMETHEUS_SERVICE}
        - --label=namespace
        - --label-value=${ns}
        - --enable-label-apis=true
        - --error-on-replace=true
        image: gcr.io/shawn-mesh-2022/prom-proxy:latest
        imagePullPolicy: IfNotPresent
        livenessProbe:
          failureThreshold: 3
          httpGet:
            path: /healthz
            port: http
            scheme: HTTP
          periodSeconds: 10
          successThreshold: 1
          timeoutSeconds: 1
        name: prom-label-proxy
        ports:
        - containerPort: 8080
          name: http
          protocol: TCP
        readinessProbe:
          failureThreshold: 3
          httpGet:
            path: /healthz
            port: http
            scheme: HTTP
          periodSeconds: 10
          successThreshold: 1
          timeoutSeconds: 1
        resources:
          limits:
            cpu: 200m
            memory: 128Mi
          requests:
            cpu: 100m
            memory: 64Mi
        securityContext:
          readOnlyRootFilesystem: true
          runAsGroup: 65534
          runAsNonRoot: true
          runAsUser: 65534
        terminationMessagePath: /dev/termination-log
        terminationMessagePolicy: File
      dnsPolicy: ClusterFirst
      restartPolicy: Always  
EOF
}

function __print_separator__ () {
  echo "------------------------------------------------------------------------------"
}

# Run the script from main()
__main__ "$@"