#!/bin/bash
source ../env.sh
openssl genrsa -out grafana-sa 2048
cat <<EOF >grafana-req.yaml 
[req]
default_bits              = 2048
req_extensions            = extension_requirements
distinguished_name        = dn_requirements
prompt                    = no

[extension_requirements]
basicConstraints          = CA:FALSE
keyUsage                  = nonRepudiation, digitalSignature, keyEncipherment
subjectAltName            = @sans_list

[dn_requirements]
0.organizationName        = shawnk8s
commonName                = *.shawnk8s.com

[sans_list]
DNS.1                     = *.shawnk8s.com
EOF

openssl req -new -key grafana-sa \
    -out cert.pem \
    -config grafana-req.yaml

openssl x509 -req \
    -signkey grafana-sa \
    -in cert.pem \
    -out csr.out \
    -extfile grafana-req.yaml \
    -extensions extension_requirements \
    -days 30
kubectl create ns ${MONITORING_NS}
kubectl create secret tls wildcard-shawnk8s-com \
  --cert=csr.out \
  --key=grafana-sa \
  -n ${MONITORING_NS}
