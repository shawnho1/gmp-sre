#!/bin/bash
export GCP_BILLING_EXPORT_TABLE_FULL_PATH=shawnho-demo-2023.cloud_billing_consumption.gcp_billing_export_v1_011D8C_2DBDF1_492309
export USAGE_METERING_PROJECT_ID=shawnho-demo-2023
export USAGE_METERING_DATASET_ID=gke_cluster_consumption
export USAGE_METERING_START_DATE=2023-01-24
export COST_BREAKDOWN_TABLE_ID=usage_metering_cost_breakdown
export USAGE_METERING_QUERY_TEMPLATE=usage_metering_query_template_request_and_consumption.sql
export USAGE_METERING_QUERY=cost_breakdown_query.sql

sed \
-e "s/\${fullGCPBillingExportTableID}/$GCP_BILLING_EXPORT_TABLE_FULL_PATH/" \
-e "s/\${projectID}/$USAGE_METERING_PROJECT_ID/" \
-e "s/\${datasetID}/$USAGE_METERING_DATASET_ID/" \
-e "s/\${startDate}/$USAGE_METERING_START_DATE/" \
"$USAGE_METERING_QUERY_TEMPLATE" \
> "$USAGE_METERING_QUERY"

bq query \
--project_id=$USAGE_METERING_PROJECT_ID \
--use_legacy_sql=false \
--destination_table=$USAGE_METERING_DATASET_ID.$COST_BREAKDOWN_TABLE_ID \
--schedule='every 24 hours' \
--display_name="GKE Usage Metering Cost Breakdown Scheduled Query" \
--replace=true \
"$(cat $USAGE_METERING_QUERY)"